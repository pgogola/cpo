% ZADANIE 7
lena_img = imread('lena.jpg');
lena_img = rgb2gray(lena_img); % jesli obrazek oryginalnie jest gray zakomentowac
figure, imshow(lena_img), title("lena");

X = fft2(lena_img);

real_X = real(X);
imag_X = imag(X);

figure, imshow(real_X), title("Lena, real(X)");
figure, imshow(imag_X), title("Lena, imag(X)");

X_s = fftshift(X);

real_X_s = real(X_s);
imag_X_s = imag(X_s);

figure, imshow(real_X_s), title("Lena, real(X_s)");
figure, imshow(imag_X_s), title("Lena, imag(X_s)");

abs_X_s = abs(X_s);
figure, imshow(abs_X_s), title("Lena, abs(X_s)");

% ZADANIE 8
%z modułu
abs_X = abs(X);
inv_abs_X = uint8(ifft2(abs_X));
figure, imshow(inv_abs_X), title("Lena, tylko moduł");

%z fazy
angle_X = angle(X);
phase_X = exp(1j*angle_X);
inv_phase_X = ifft2(phase_X);

inv_phase_X_min = min(min(abs(inv_phase_X)));
inv_phase_X_max = max(max(abs(inv_phase_X)));

figure, ...
    imshow(abs(inv_phase_X), [inv_phase_X_min inv_phase_X_max]), ...
    colormap gray, title("Lena, tylko faza");