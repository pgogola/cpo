% ZADANIE 1
% a)
N_1 = 480;
N_2 = 640;

k_1 = 3;
k_2 = 4;

x = zeros(N_1, N_2);

for n1 = 1:N_1
    for n2 = 1:N_2
        x(n1, n2) = exp(1j*(2*pi*k_1*n1/N_1))*exp(1j*(2*pi*k_2*n2/N_2));
    end
end

% b)
figure, imshow(real(x)), title("Zad 1b) real(x)");
figure, imshow(imag(x)), title("Zad 1b) imag(x)");


% ZADANIE 2
% a)
X = fft2(x);

% b)
real_X = real(X);
imag_X = imag(X);

figure, imshow(real_X), title("Zad 2b) real(X)");
figure, imshow(imag_X), title("Zad 2b) imag(X)");

% c)
X_s = fftshift(X);

% d) skalowanie osi?
real_X_s = real(X_s);
imag_X_s = imag(X_s);

figure, imshow(real_X_s), title("Zad 2d) real(X_s)");
figure, imshow(imag_X_s), title("Zad 2d) imag(X_s)");

% e) okreslic liczbe prążków
abs_X_s = abs(X_s);
figure, imshow(abs_X_s), title("Zad 2e) abs(X_s)");

% ZADANIE 3
% a)
N_1 = 480;
N_2 = 640;

k_1 = 4;
k_2 = 5;

y = zeros(N_1, N_2);

y(k_1, k_2) = 1; % 2D delta dirac

% b)
figure, imshow(real(y)), title("Zad 3b) real(y)");
figure, imshow(imag(y)), title("Zad 3b) imag(y)");

% ZADANIE 4
% a)
Y = fft2(y);

% b)
real_Y = real(Y);
imag_Y = imag(Y);

figure, imshow(real_Y), title("Zad 3b) real(Y)");
figure, imshow(imag_Y), title("Zad 4b) imag(Y)");

% c)
Y_s = fftshift(Y);

% d) skalowanie osi?
real_Y_s = real(Y_s);
imag_Y_s = imag(Y_s);

figure, imshow(real_Y_s), title("Zad 4d) real(Y_s)");
figure, imshow(imag_Y_s), title("Zad 4d) imag(Y_s)");

% e) okreslic liczbe prążków
abs_Y_s = abs(Y_s);
figure, imshow(abs_Y_s), title("Zad 4e) abs(Y_s)");
