lena_img = imread('lena.jpg');
lena_img = rgb2gray(lena_img); % jesli obrazek oryginalnie jest gray zakomentowac

lena_size = size(lena_img);
ones_matrix = zeros(10, 10);

size_ = 4;
x_shift = uint8((10 - size_)/2);
y_shift = uint8((10 - size_)/2);

for x = x_shift:x_shift+size_+1
    for y = y_shift:y_shift+size_+1
        ones_matrix(x, y) = 1;
    end
end
figure, imshow(ones_matrix);
sum(sum(ones_matrix));