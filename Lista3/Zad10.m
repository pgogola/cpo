%{
lena_img = imread('lena.jpg');
lena_img = rgb2gray(lena_img); % jesli obrazek oryginalnie jest gray zakomentowac
%lena_img = rescale(lena_img);
%figure, imshow(lena_img), title("lena");

size_ = 50;
ones_matrix = ones(size_)./(size_*size_);

lena_ones = uint8(conv2(lena_img, ones_matrix));
figure, imshow(lena_ones), title("lena");
%}

%http://aragorn.pb.bialystok.pl/~boldak/DIP/CPO-W06-v05-50pr.pdf
lena_img = imread('lena.jpg');
lena_img = rgb2gray(lena_img); % jesli obrazek oryginalnie jest gray zakomentowac
lena_img = rescale(lena_img);
lena_size = size(lena_img);

figure, imshow(lena_img);
%transformata i przesunięcie
lena_fft = fft2(lena_img);
lena_fft = fftshift(lena_fft);
figure, imshow(abs(lena_fft));


%przygotowujemy filtr
ones_matrix = zeros(lena_size);
size_ = 50;
x_shift = (lena_size(1) - size_)/2;
y_shift = (lena_size(2) - size_)/2;

for x = x_shift:x_shift+size_+1
    for y = y_shift:y_shift+size_+1
        ones_matrix(x, y) = 1/(size_*size_);
    end
end

%transformata i przesunięcie
ones_fft = fft2(ones_matrix);
ones_fft = fftshift(ones_fft);
figure, imshow(abs(ones_fft));

lena_filtr = lena_fft.*abs(ones_fft);

lena_ifft = ifftshift(lena_filtr);
lena = ifft2(lena_filtr);
figure, imshow(lena), title("lena_fft");


