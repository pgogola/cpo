% zwykla korelacja
image_rgb = imread("koperta.png");
pattern_rgb = imread("znaczek.png");

image = rgb2gray(image_rgb);
pattern = rgb2gray(pattern_rgb);

result_classic = classic_correlation(image, pattern);
figure, imshow(result_classic);
maximum = max(result_classic);
maximum = max(maximum);
[x, y] = find(result_classic==maximum);
xoffSet = size(pattern,2);
yoffSet = size(pattern,1);
x = x-xoffSet;
y = y-yoffSet;

figureimshow(image_rgb);
drawrectangle( ...
    gca, ...
    'Position', ...
    [x, y, size(pattern, 2), size(pattern, 1)], ...
    'FaceAlpha', ...
    0 ...
);

%korelacja fazowa
image_rgb = imread("koperta.png");
pattern_rgb = imread("znaczek.png");

image = rgb2gray(image_rgb);
pattern = rgb2gray(pattern_rgb);

result_classic = phaze_correlation(image, pattern);
figure, imshow(result_classic);
maximum = max(result_classic);
maximum = max(maximum);
[x, y] = find(result_classic==maximum);


figure, imshow(image_rgb);
drawrectangle( ...
    gca, ...
    'Position', ...
    [x, y, size(pattern, 2), size(pattern, 1)], ...
    'FaceAlpha', ...
    0 ...
);

% funkcje:
function [x] = phaze_correlation(image, pattern)
%https://www.ee.pw.edu.pl/~jarek/Estymacja/phaseCorrelation.htm
%https://www.mathworks.com/matlabcentral/answers/106958-phase-correlation-to-detect-shift
    padx=size(image, 1)-size(pattern, 1);
    pady=size(image, 2)-size(pattern, 2);
    pattern = padarray(pattern, [padx pady], 'replicate','post');

    image_ft = fft2(image);
    pattern_ft = fft2(pattern);
    
    pattern_ft_c=conj(pattern_ft);
    x=image_ft.*pattern_ft_c./abs(image_ft.*pattern_ft_c);
    x=ifft2(x);
end

function [x] = classic_correlation(image, pattern)
    image_normalized = rescale(image);
    pattern_normalized = rescale(pattern);
    x = corr2(pattern_normalized, image_normalized);
end
