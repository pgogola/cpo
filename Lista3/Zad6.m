% ZADANIE 6
N_1 = 480;
N_2 = 640;

k_1 = 3;
k_2 = 4;

x = zeros(N_1, N_2);

for n1 = 1:N_1
    for n2 = 1:N_2
        x(n1, n2) = 1+cos(2*pi*k_1*n1/N_1+2*pi*k_2*n2/N_2);
    end
end

figure, imshow(real(x)), title("real(x)");
figure, imshow(imag(x)), title("imag(x)");

X = fft2(x);

real_X = real(X);
imag_X = imag(X);

figure, imshow(real_X), title("real(X)");
figure, imshow(imag_X), title("imag(X)");

X_s = fftshift(X);

real_X_s = real(X_s);
imag_X_s = imag(X_s);

figure, imshow(real_X_s), title("real(X_s)");
figure, imshow(imag_X_s), title("imag(X_s)");

abs_X_s = abs(X_s);
figure, imshow(abs_X_s), title("abs(X_s)");
