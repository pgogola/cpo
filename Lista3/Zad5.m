% ZADANIE 5
N_1 = 480;
N_2 = 640;

% first for f1
k_1 = 3; k_2 = 4;
do_all(N_1, N_2, k_1, k_2, @function1, "exp(1j*(2*pi*k_1*n1/N_1))*exp(1j*(2*pi*k_2*n2/N_2));");

% second for f1
k_1 = 7; k_2 = 6;
do_all(N_1, N_2, k_1, k_2, @function1, "exp(1j*(2*pi*k_1*n1/N_1))*exp(1j*(2*pi*k_2*n2/N_2));");

% third for f1
k_1 = 30; k_2 = 42;
do_all(N_1, N_2, k_1, k_2, @function1, "exp(1j*(2*pi*k_1*n1/N_1))*exp(1j*(2*pi*k_2*n2/N_2));");

% first for f2
k_1 = 3; k_2 = 4;
do_all(N_1, N_2, k_1, k_2, @function2, "x(n1, n2) = dirac_delta(n1-k1, n2-k2);");

% second for f2
k_1 = 7; k_2 = 6;
do_all(N_1, N_2, k_1, k_2, @function2, "x(n1, n2) = dirac_delta(n1-k1, n2-k2);");

% third for f2
k_1 = 30; k_2 = 42;
do_all(N_1, N_2, k_1, k_2, @function2, "x(n1, n2) = dirac_delta(n1-k1, n2-k2);");


% functions at the end of file

function [x] = function1(N_1, N_2, k_1, k_2)
    x = zeros(N_1, N_2);
    for n1 = 1:N_1
        for n2 = 1:N_2
            x(n1, n2) = exp(1j*(2*pi*k_1*n1/N_1))*exp(1j*(2*pi*k_2*n2/N_2));
        end
    end
end

function [x] = function2(N_1, N_2, k_1, k_2)
    x = zeros(N_1, N_2);
    for n1 = 1:N_1
        for n2 = 1:N_2
            if n1-k_1 == 0 && n2-k_2 == 0
                x(n1, n2) = 1;
            end
        end
    end
end

function [] = do_all(N_1, N_2, k_1, k_2, fun, fun_desc)
    % Cz1
    x = fun(N_1, N_2, k_1, k_2);
    figure, imshow(real(x)), title(strcat("real(x), x=", fun_desc));
    figure, imshow(imag(x)), title(strcat("imag(x), x=", fun_desc));
    
    % Cz2
    X = fft2(x);
    
    real_X = real(X);
    imag_X = imag(X);
    
    figure, imshow(real_X), title(strcat("real(X), X=fft(x), x=", fun_desc));
    figure, imshow(imag_X), title(strcat("real(X), X=fft(x), x=", fun_desc));
    
    X_s = fftshift(X);
    
    % skalowanie osi
    real_X_s = real(X_s);
    imag_X_s = imag(X_s);
    
    figure, imshow(real_X_s), title(strcat("real(X_s), X_s=fftshift(fft(x)), x=", fun_desc));
    figure, imshow(imag_X_s), title(strcat("imag(X_s), X_s=fftshift(fft(x)), x=", fun_desc));
    
    abs_X_s = abs(X_s);
    figure, imshow(abs_X_s), title(strcat("abs(X_s), X_s=fftshift(fft(x)), x=", fun_desc));
end
