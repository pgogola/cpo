clear;

%% ZADANIE 3
%ladowanie obrazka i konwersja na 'gray'
img_rgb = imread('lena.jpg');
img_gray = rgb2gray(img_rgb);
x = img_gray;
% zadanie wykonane na podstawie wzorów z:
% http://wmii.uwm.edu.pl/~mario/jsmallfib_top/upload/wdgm/zestaw5.pdf

% a. zwęzić histogram obrazu, tj. utworzyć nowy obraz, którego histogram
% jest węższy niż histogram obrazu,
original_hist = imhist(x);

target_img_max = 150;
target_img_min = 100;

a = (target_img_max-target_img_min)/255;
b = target_img_min;
y = uint8(a.*x+b);
constricted_hist = imhist(y);

figure;
subplot(2,1,1);
plot(original_hist)

subplot(2,1,2); 
plot(constricted_hist);

% b. rozszerzyć ('rozciągnąć') maksymalnie histogram obrazu (wynik p 3a),
% tj. utworzyć nowy obraz, którego histogramu nie da się już bardziej 
% rozciągnąć przy użyciu funkcji liniowej
y_max = max(max(y));
y_min = min(min(y));

v = (255/(y_max-y_min)).*(y-y_min);
% to ponizej to jest rozbicie powyzszego wyrazenia do postaci y = ax+b ale
% nie wiem co jest grane bo zawsze liczy mi a i b jako uint :O
% a = double(255)/(y_max-y_min);
% b = (double(255)*y_min)/(y_max-y_min);
% v = uint8(a.*y-b); %check it

expanded_hist = imhist(v);

figure;
subplot(2,1,1);
plot(constricted_hist)

subplot(2,1,2); 
plot(expanded_hist);

%% ZADANIE 4
% a)
y_distribution = image_distribution(y);
lut_y = look_up_table(y_distribution);
im_y = lut_y(y+1); % musimy dodac 1 bo tablice w matlabie numerowane są od 1, a wartości zaczynają sie od 0
figure, imshow(im_y), title("Histogram wyrównany y");

% b)
x_distribution = image_distribution(x);
lut_x = look_up_table(x_distribution);
im_x = lut_x(x+1); % musimy dodac 1 bo tablice w matlabie numerowane są od 1, a wartości zaczynają sie od 0
figure, imshow(im_x), title("Histogram wyrównany x");

% c)
v_distribution = image_distribution(v);
lut_v = look_up_table(v_distribution);
im_v = lut_v(v+1); % musimy dodac 1 bo tablice w matlabie numerowane są od 1, a wartości zaczynają sie od 0
figure, imshow(im_v), title("Histogram wyrównany v");