function [x] = look_up_table(distribution)
    D_non_zero = distribution > 0;
    Dmin_idx = find(D_non_zero, 1);
    Dmin = distribution(Dmin_idx);
    x = uint8(floor(((distribution-Dmin)./(1-Dmin)).*255));
end

