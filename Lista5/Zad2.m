clear;

%% ZADANIE 2
%ladowanie obrazka i konwersja na 'gray'
img_rgb = imread('lena.jpg');
img_gray = rgb2gray(img_rgb);

% a. wyznaczyć wartość średnią obrazu
img_mean = mean(img_gray, 'all');

% b. wyznaczyć odchylenie standardowe/wariancję obrazu
img_std = std(double(img_gray), 1, 'all');
img_var = var(double(img_gray), 1, 'all');

% c. wyznaczyć histogram obrazu ,
    % i) z użyciem funkcji imhist()
hist_built_in = imhist(img_gray);
figure, plot(hist_built_in);
    % ii) z użyciem własnej procedury według algorytmu z wykładu.
hist_built_in = histogram(img_gray);
figure, plot(hist_built_in);

    
%% Functions
function[x] = histogram(img)
    x = zeros(256);
    img_size = size(img);
    for i=1:img_size(1)
        for j=1:img_size(2)
            x(img(i, j)) = x(img(i, j)) + 1;
        end
    end
end