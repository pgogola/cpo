function [x] = image_distribution(image)
    h = imhist(image);
    x = cumsum(h, 1); 
    x = x./x(end);
end

