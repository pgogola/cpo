clear;

%% ZADANIE 5
%ladowanie obrazka i konwersja na 'gray'
img_rgb = imread('lena.jpg');
% zadanie wykonane na podstawie wzorów z:
% http://wmii.uwm.edu.pl/~mario/jsmallfib_top/upload/wdgm/zestaw5.pdf

result_img = histogram_equalization_rgb(img_rgb);
figure, imshow([img_rgb, result_img]), title("Obraz oryginalny, obraz po rozciaganiu histogramu")


function [x] = histogram_equalization_rgb(img)
    img_size = size(img);
    x = zeros(img_size);
    for i=1:img_size(3)
        distribution = image_distribution(img(:,:,i));
        lut = look_up_table(distribution);
        x(:,:,i) = lut(img(:,:,i)+1);
    end
end