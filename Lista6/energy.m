function[E] = energy(img)
%https://www.researchgate.net/profile/Andrzej_Materka/publication/256079247_Wstep_do_komputerowej_analizy_obrazow/links/00b7d52174f92803c2000000/Wstep-do-komputerowej-analizy-obrazow.pdf
% ze strony 147 wyciagnałem sobie liczenie energii jak zostalo tam
% wspomniane do odzielnej funckji
    img_s = img.^2;
    E = sum(img_s(:));
end