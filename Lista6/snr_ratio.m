function[G] = snr_ratio(input_img, output_img)
    [input_snr, ~] = signal_noise_ratio(input_img); % liczymy SNR obrazka wejsciowego
    [output_snr, ~] = signal_noise_ratio(output_img); % liczymy SNR obrazka wyjsciowego
    
    G = 10*log(output_snr/input_snr); % obliczamy na ich podstawie G
end
