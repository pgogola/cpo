function[SNR, SNR_log] = signal_noise_ratio(img_in)
%https://www.researchgate.net/profile/Andrzej_Materka/publication/256079247_Wstep_do_komputerowej_analizy_obrazow/links/00b7d52174f92803c2000000/Wstep-do-komputerowej-analizy-obrazow.pdf
% ze strony 147
    mean_val = mean(img_in(:));
    image_energy = energy(img_in);
    diff = img_in - mean_val;
    diff_energy = energy(diff);
    SNR = image_energy/diff_energy;
    SNR_log = 10*log(SNR);
end