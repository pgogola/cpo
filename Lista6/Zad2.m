clear all;

%% ZAD 2
% a)
%image_size = 512;
%image = uint8(255.*(checkerboard(60,4,4)>0.5));
image = imread('lena.jpg');
image = rgb2gray(image);

noised_image = salt_pepper_noise(image, 0.2, 0.5);
t = sprintf( ...
    'Obraz oryginalny,\nobraz z zaklóceniem sol-pieprz. Zakłócane piksele=%f, szum typu sol=%f', ...
    0.2, 0.5 ...
);
figure, imshow([image, noised_image]), title(t);

% b)
filtered_img = median_filter(noised_image, 5);
t = sprintf( ...
    'Obraz zakłócony, obraz po filtracji (filtr medianowy), wielkoscia filtra=%d', ...
    5 ...
);
figure, imshow([noised_image, filtered_img]), title(t);

% c, d, e)
point_c_d_e('lena.jpg', 0.2, 0.5, 3);
point_c_d_e('lena.jpg', 0.2, 0.5, 5);
point_c_d_e('lena.jpg', 0.2, 0.5, 9);

point_c_d_e('lena.jpg', 0.4, 0.5, 3);
point_c_d_e('lena.jpg', 0.6, 0.2, 3);
point_c_d_e('lena.jpg', 0.6, 0.2, 5);

% f)
iteration_filtering_median('lena.jpg', 0.6, 0.2, 5, 10)
iteration_filtering_median('lena.jpg', 0.3, 0.5, 5, 10)
iteration_filtering_median('lena.jpg', 0.3, 0.2, 5, 10)
iteration_filtering_median('lena.jpg', 0.6, 0.7, 5, 10)

% g)
iteration_filtering_median_improv('lena.jpg', 0.6, 0.2, 5, 10, 20)
iteration_filtering_median_improv('lena.jpg', 0.3, 0.5, 5, 10, 20)
iteration_filtering_median_improv('lena.jpg', 0.3, 0.2, 5, 10, 20)
iteration_filtering_median_improv('lena.jpg', 0.6, 0.7, 5, 10, 20)

%% FUNKCJE
function[img_out] = salt_pepper_noise(img_in, noised_proportion, salt_proportion)
    %noised_proportion - jaka czesc pikseli ma byc zaklócona
    %salt_proportion - jaka czesc zakłóconych pikseli ma byc typu "sol"

    number_elements = numel(img_in); %liczymy ile pikseli jest w obrazie
    pepper_proportion = 1-salt_proportion; %obliczamy jaka czesc zaklócanych 
    % pikseli ma byc typu pieprz
    
    noised_elements = noised_proportion*number_elements; % tyle pikseli ma byc zaklóconych
    
    salt_pixels = floor(salt_proportion * noised_elements); % tyle pikseli ma byc szumem typu "sol"
    pepper_pixels = floor(pepper_proportion * noised_elements); % tyle pikseli ma byc szumem typu "pieprz"
    
    selected_pixels = randperm(number_elements); %robimy losowa kolejnosc indeksow w obrazie
    
    %ponizej jest wykorzystywane cos takiego jak linear indexing:
    % https://www.mathworks.com/company/newsletters/articles/matrix-indexing-in-matlab.html
    
    img_out = img_in;
    img_out(selected_pixels(1:salt_pixels)) = 255; % odpowiednia liczbe pikseli nadpisujemy szumem typu "sol"
    img_out(selected_pixels(salt_pixels:salt_pixels+pepper_pixels)) = 0; % i analogicznie pieprz
end

function[img_out] = median_filter(img_in, filter_size)
    img_out = uint8(zeros(size(img_in)));
    borders = floor(filter_size/2);
    
    % jesli filter size == 5 to dla pixela X o wspolrzednych [i, j] obliczamy wartosc z prostokąta
    % wielkosci 5x5:
    %{
         | | | | | |
         | | | | | |
         | | |X| | |
         | | | | | |
         | | | | | |
    %}
    
    for i=1:size(img_in, 1)
        for j=1:size(img_in, 2)
            i_min = i - borders;
            i_max = i + borders;
            j_min = j - borders;
            j_max = j + borders;
            
            result_pix = 0;
            % tutaj sprawdzamy czy caly kwadracik jest juz w obrazku i
            % jesli tak to liczymy mediane z tego kwadratu
            if i_min < 1 || i_max>size(img_in, 1) || j_min < 1 || j_max>size(img_in, 2)
                result_pix = img_in(i, j);
            else
                sub_img = img_in(i_min:i_max, j_min:j_max);
                result_pix = median(sub_img(:));
            end
            
            img_out(i, j) = result_pix;
        end
    end
end

function[img_out] = median_filter_improv(img_in, filter_size, T)
    img_out = uint8(zeros(size(img_in)));
    borders = floor(filter_size/2);
    
    for i=1:size(img_in, 1)
        for j=1:size(img_in, 2)
            i_min = i - borders;
            i_max = i + borders;
            j_min = j - borders;
            j_max = j + borders;
            
            result_pix = 0;
            if i_min < 1 || i_max>size(img_in, 1) || j_min < 1 || j_max>size(img_in, 2)
                result_pix = img_in(i, j);
            else
                sub_img = img_in(i_min:i_max, j_min:j_max);
                result_pix = median(sub_img(:));
                % do tej pory to samo a ponizej jest korekcja z
                % dopuszczalnym marginesem wartosci (patrz wyklad)
                if abs(result_pix-img_in(i, j)) < T
                    result_pix = img_in(i, j);
                end
            end
            
            img_out(i, j) = result_pix;
        end
    end
end

function[] = point_c_d_e(image_file, noised_proportion, salt_proportion, filter_size)
    image = imread(image_file);
    image = rgb2gray(image);
    
    noised_image = salt_pepper_noise(image, noised_proportion, salt_proportion);
    filtered_img = median_filter(noised_image, filter_size);
    
    [~, SNR_in] = signal_noise_ratio(noised_image);
    [~, SNR_out] = signal_noise_ratio(filtered_img);
    G = snr_ratio(noised_image, filtered_img);
    t = sprintf("Obraz oryginalny,\nObraz szumem np=%f, sp=%f,\nObraz po filtracji\nSNR_in=%f dB, SNR_out=%f dB, G=%f dB", ...
        noised_proportion, salt_proportion, SNR_in, SNR_out, G ...
    );
    figure, imshow([image, noised_image, filtered_img]), title(t);
end

function[g_by_iterations] = iteration_filtering_median(image_file, noised_proportion, salt_proportion, filter_size, iterations)
    image = imread(image_file);
    image = rgb2gray(image);
    
    noised_image = salt_pepper_noise(image, noised_proportion, salt_proportion);
    result = noised_image;
    g_by_iterations = size(iterations);
    for i=1:iterations
        result = median_filter(result, filter_size);
        g_by_iterations(i) = snr_ratio(noised_image, result);
    end
    t = sprintf("Obraz oryginalny,\nObraz szumem np=%f, sp=%f,\nObraz po filtracji", ...
        noised_proportion, salt_proportion ...
    );
    figure, imshow([image, noised_image, result]), title(t);
end

function[g_by_iterations] = iteration_filtering_median_improv(image_file, noised_proportion, salt_proportion, filter_size, iterations, T)
    image = imread(image_file);
    image = rgb2gray(image);
    
    noised_image = salt_pepper_noise(image, noised_proportion, salt_proportion);
    result = noised_image;
    g_by_iterations = size(iterations);
    for i=1:iterations
        result = median_filter_improv(result, filter_size, T);
        g_by_iterations(i) = snr_ratio(noised_image, result);
    end
    t = sprintf("Obraz oryginalny,\nObraz szumem np=%f, sp=%f,\nObraz po filtracji", ...
        noised_proportion, salt_proportion ...
    );
    figure, imshow([image, noised_image, result]), title(t);
end
