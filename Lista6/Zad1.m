clear all;

%% ZAD 1
% a)
% uzywamy imadjust w imshow, aby byly widoczne zaklucenia (obraz w
% przedziale 0-1)

image = double(checkerboard(60,4,4)>0.5); % tworzymy szachownice

SNR = 10; % docelowy SNR (nie jestem pewien czy poprawnie jest to uzyte)
noised_image = add_noise(image, SNR);
t = sprintf( ...
    'Obraz oryginalny,\nobraz z zaklóceniem SNR %f', ...
    SNR...
);
figure, imshow([image, imadjust(noised_image)]), title(t);

% b)
% filtr bilateralny to np. filtr gaussa
% jesli dobrze rozumiem dzialanie imbilatfilt to wlasnei to są te
% parametry:
% sigma_s - istotność sąsiedztwa próbek
%sigma_b (teraz widze ze w prezentacji jest p) - istotność podobieństwa próbek
sigma_s = 4;
sigma_b = 4;
filtered_image = imbilatfilt(noised_image, sigma_s, sigma_b);
t = sprintf( ...
    'Obraz z zaklóceniem SNR %f,\nObraz przefiltrowany filtrem bilateralnym (sigma_s=%f, sigma_b=%f)', ...
    SNR, sigma_s, sigma_b...
);
figure, imshow([imadjust(noised_image), imadjust(filtered_image)]), title(t);

% c)
G_snr10_sigmas4_sigmab4 = point_c_all(10, 4, 4)
G_snr2_sigmas4_sigmab4 = point_c_all(2, 4, 4)
G_snr2_sigmas20_sigmab20 = point_c_all(2, 20, 20)

% d)
G_iterations_snr2_sigmas4_sigmab20 = point_d_all(2, 4, 20, 10)
G_iterations_snr2_sigmas20_sigmab4 = point_d_all(2, 20, 4, 10)


%% FUNKCJE
function[noised_image] = add_noise(image, snr)
    %Wzór z https://www.mathworks.com/help/signal/ref/snr.html
    noise = randn(size(image)).*std(image)/db2mag(snr); % generujemy sygnał szumu
    noised_image = image + noise; % szum dodawany jest do obrazka wejsciowego
end

function[G] = point_c_all(SNR, sigma_s, sigma_b)
    image = double(checkerboard(60,4,4)>0.5);

    noised_image = add_noise(image, SNR);
    t = sprintf( ...
        'Obraz oryginalny,\nobraz z zaklóceniem SNR %f', ...
        SNR...
    );
    figure, imshow([image, imadjust(noised_image)]), title(t);
    
    % imbilatfilt: https://au.mathworks.com/help/images/ref/imbilatfilt.html
    filtered_image = imbilatfilt(noised_image, sigma_s, sigma_b);
    G = snr_ratio(noised_image, filtered_image);
    t = sprintf( ...
        'Obraz oryginalny,\nobraz z zaklóceniem SNR %f,\nobraz po filtracji sigma_s=%f, sigma_b=%f. G = %f', ...
        SNR, sigma_s, sigma_b, G ...
    );
    figure, imshow([imadjust(image), imadjust(noised_image), imadjust(filtered_image)]), title(t);
end

function[G_by_iterations] = point_d_all(SNR, sigma_s, sigma_b, iterations)
    image = double(checkerboard(60,4,4)>0.5);

    noised_image = add_noise(image, SNR);
    t = sprintf( ...
        'Obraz oryginalny,\nobraz z zaklóceniem SNR %f', ...
        SNR ...
    );
    figure, imshow([image, imadjust(noised_image)]), title(t);
    G_by_iterations = size(iterations);
    filtered_image = noised_image;
    % zadanie dotyczylo filtracji w petli i porownania jak za kazdym razem
    % zmieni sie wynik. Z tego powodu w kazdej iteracji filtrujemy obraz a
    % nastepnie wyliczamy wspolczynnik G. Zakomentowalem wyswietlanie
    % wyniku po kazdej iteracji zeby nie robic syfu, ale mozna odkomentowac
    % i zobaczyc wyniki z kazda iteracja :)
    for i = 1:iterations
        filtered_image = imbilatfilt(filtered_image, sigma_s, sigma_b);
        G = snr_ratio(noised_image, filtered_image);
        G_by_iterations(i) = G;
        %{
        t = sprintf( ...
            'Obraz oryginalny,\nobraz z zaklóceniem SNR %f,\nobraz po filtracji sigma_s=%f, sigma_b=%f. G = %f', ...
            SNR, sigma_s, sigma_b, G ...
        );
        figure, imshow([imadjust(image), imadjust(noised_image), imadjust(filtered_image)]), title(t);
        %}
    end
end