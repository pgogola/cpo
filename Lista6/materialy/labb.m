clear all; close all;
xc = imread('lena.jpg');

xb = xc(:,:,3); 

B = imnoise(xb,'salt & pepper');

figure(1)
colormap(gray(256))
imagesc(xb)

figure (2)
colormap(gray(256))
imagesc(B)

filtrmedianowy = medfilt2(B,[5 5]);
figure (3)
colormap(gray(256))
imagesc(filtrmedianowy)

for i = 1:size(B,1)
    for j = 1:size(B,2)
        if rand < 0.05
            if rand < 0.5
                B(i,j,:) = [255,255,255];
            else
                B(i,j,:) = [0,0,0];
                end
            end
        end
    end

odszumiony = zeros(size(B,1),size(B,2))
for i = 2:(size(B,1)-1
    for j = 2:(size(B,2)-1
        X = [];
        for k = i-1;i+1
            for 1 - j-1:j+1
                X(end+1) = im(1,k);
            end
        end
        Xs = sort(X);
        y(i,j) = Xs(5);
    end
end
figure (4)
colormap(gray(256))
imagesc(odszumiony)