close all;
clear all;

obraz = imread("lena.png");
obrazbw = obraz(:,:,3);
obrazbwszum=obrazbw;
colormap(gray(256));

zaszumienie=0.04;
otoczenie=3;
TEMP=zeros(otoczenie+1,otoczenie+1);

%zaszumianie
for i=1:512
    for j=1:512
        if(rand()<zaszumienie) obrazbwszum(i,j)=1; end
        if(rand()<zaszumienie) obrazbwszum(i,j)=255; end
    end
    end

subplot(1,3,1);
imagesc(obrazbw);

subplot(1,3,2);
imagesc(obrazbwszum);

%biale
for i=1+otoczenie:512-otoczenie
    for j=1+otoczenie:512-otoczenie
        if (obrazbwszum(i,j)==255) TEMP=obrazbwszum(i-otoczenie:i+otoczenie,j-otoczenie:j+otoczenie);
            obrazbwszum(i,j)=median(TEMP,'all');
        end
    end
end
 
%czarne
for i=1+otoczenie:512-otoczenie
    for j=1+otoczenie:512-otoczenie
        if (obrazbwszum(i,j)==1) TEMP=obrazbwszum(i-otoczenie:i+otoczenie,j-otoczenie:j+otoczenie);
            obrazbwszum(i,j)=median(TEMP,'all');
        end
    end
end

subplot(1,3,3);
imagesc(obrazbwszum);
