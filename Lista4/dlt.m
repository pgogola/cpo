function y=dlt(number)
    % generuje 'funkcję' delty Kronecker'a
    orig=ceil(number/2); y=zeros(number);
    y(orig,orig)=1;
end

