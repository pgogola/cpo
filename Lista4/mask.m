function y=mask(cofffreq,rank)
    %mask generuje odpowiedz impulsowa filtru 2D
    % wymaga coffreq=granica filtru (0-pi), rank=rzad filtru
    rofs=dzd(rank);
    y=cofffreq*besselj(1,cofffreq*rofs)./(2*pi*rofs);
end 