 +++++++++++++++++++++++++++++
function y=mask(cofffreq,rank)
%mask generuje odpowiedz impulsowa filtru 2D
% wymaga coffreq=granica filtru (0-pi), rank=rzad filtru
rofs=dzd(rank);
y=cofffreq*bessel(1,cofffreq*rofs)./(2*pi*rofs);

2. +++++++++++++++++++++++++++++
function  y=dzd(number)
% dzd generuje argument w postaci macierzy
mr=ones(number);
orig=ceil(number/2);
for k=1:number
 mr(k,:)=(k-orig)*mr(k,:);
end
mc=mr';
y=sqrt(mr.^2+mc.^2);
y(orig,orig)=eps;

3. +++++++++++++++++++++++++++++
function y=rw2d(wind1d)
%rw2d generuje okno 2D na podstawie okna 1D
z=wind1d;
y=z*z';

4. +++++++++++++++++++++++++++++
function y=dlt(number)
% generuje 'funkcję' delty Kronecker'a
orig=ceil(number/2); y=zeros(number);
y(orig,orig)=1;