%% ZADANIE 7

img = rgb2gray(imread("lena.jpg"));
rank = 40;

% zrobimy filtrację co 0.1*pi - granicę odciecia

for i = 0:0.1:1
    lpr = filter2(low_pass_filter(i*pi, rank), img); % filtr dolnoprzepustowy
    hpr = filter2(high_pass_filter(i*pi, rank), img); % filtr gornoprzepustowy
    figure, imshow([img, lpr, hpr]), title( ...
        strcat( ...
            strcat( ...
                "original, low pass filter, high pass filter (freq ratio = ", ...
                num2str(i) ...
            ), ...
            ")" ...
        ) ...
    );
end

%% ZADANIE 8 (jak rozumiem tu chodzi o takie "ręczne usuwanie" czestotliwości)

frequency_ratio = 0.3;
img_fft = fft2(img);
img_fft_s = fftshift(img_fft);

img_size = size(img_fft_s);
assert(img_size(1) == img_size(2), "Obraz musi być kwadratem") % to jest drobne uproszczenie
img_size = img_size(1);
half_img_size = img_size/2;

pos = [half_img_size half_img_size];
r = half_img_size*frequency_ratio;

figure, imagesc(abs(img_fft_s)), title("abs(img), maska binarna"); %potrzebujemy do nastepnej funckji
circ = drawcircle('Center', pos, 'Radius', r);
binary_mask = createMask(circ);

img_fft_s_filtered_low_pass = img_fft_s.*binary_mask;
img_fft_filtered_low_pass = ifftshift(img_fft_s_filtered_low_pass);
img_filtered_low_pass = uint8(real(ifft2(img_fft_filtered_low_pass)));

img_fft_s_filtered_high_pass = img_fft_s.*(1.-binary_mask);
img_fft_filtered_high_pass = ifftshift(img_fft_s_filtered_high_pass);
img_filtered_high_pass = uint8(real(ifft2(img_fft_filtered_high_pass)));
figure, imshow([img, img_filtered_low_pass, img_filtered_high_pass]), title( ...
        strcat( ...
            strcat( ...
                "original, low pass filter, high pass filter (freq ratio = ", ...
                num2str(frequency_ratio) ...
            ), ...
            ")" ...
        ) ...
    );


%% funkcje pomocnicze
function [x] = low_pass_filter(frequency, filter_rank)
    x = mask(frequency, filter_rank);
end

function [x] = high_pass_filter(frequency, filter_rank)
    lpf = low_pass_filter(frequency, filter_rank);
    all_pass_filter = mask(pi, filter_rank);
    x = all_pass_filter - lpf;
end