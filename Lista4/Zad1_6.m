%% ZADANIE 1
% a)
N1 = 512;
N2 = 512;
k1 = 3;
k2 = 4;
k3 = 30;
k4 = 4;

x = zeros(N1, N2);

for n1 = 1:N1
    for n2 = 1:N2
        x(n1, n2) = x1(n1, n2, k1, k2, N1, N2) + x2(n1, n2, k3, k4, N1, N2);
    end
end

% b)
figure, imshow(x), title("x(n1, n2)")

%% ZADANIE 2
% a)
X = fft2(x); %transformata Fouriera
% b)
X_s = fftshift(X); %przesuniecie
figure, imshow(abs(X_s)), title("abs(fftshift(fft2(x)))") %wyswietlamy moduł

%% ZADANIE 3 I 5 (projektowanie + filtracja)
% czestotliwosci skladowej x1
omegak1 = (2*pi*k1)/N1;
omegak2 = (2*pi*k2)/N2;

frequencyk1 = omegak1/(2*pi);
frequencyk2 = omegak2/(2*pi);

% czestotliwosci skladowej x2
omegak3 = (2*pi*k3)/N1;
omegak4 = (2*pi*k4)/N2;

frequencyk3 = omegak3/(2*pi);
frequencyk4 = omegak4/(2*pi);

%{
Z wykresu abs(fftshift(fft2(x))) widać,punkty, pokazujące wystepujące
w obrazie częstotliwości. Jest ich 4, bo przesunęlismy obraz funckją
fftshift, składowa x2 "ma" te dwa punkty dalej środka obrazu. Musimy
utworzyć filtr, który przepuści nam wysokie częstotliwości (tj skladową
x1), a zatrzyma wysokie - skladowa x2. Można zauważyc te czestotliwosci po
uruchomieniu skryptu w zakladce "workspace" - fruequncykX, gdzie X odnosi
sie do konkretnego k. Poniżej wyznaczamy sobie średnią częstotliwość będącą
naszą częstotliwością graniczna.
%}

middle_frequency = abs(frequencyk3-frequencyk1)/2;

%poniewaz w obrazie czestotliwosci są w 0-1, a funckaj wymaga 0-pi wiec
%skalujemy poniżej otrzymane middle_frequency
filter_rank = 400;
low_pass_filter = mask(middle_frequency*pi, filter_rank);

x1_only = zeros(N1, N2);
for n1 = 1:N1
    for n2 = 1:N2
        x1_only(n1, n2) = x1(n1, n2, k1, k2, N1, N2);
    end
end
figure, imshow([x, filter2(low_pass_filter,x), x1_only]), title("x, x bez skladowej x2, x1 only");


%% ZADANIE 4
kronecker_delta = dlt(512);
impulse_response = conv2(kronecker_delta, low_pass_filter, "same");
figure, imshow(impulse_response);

ir = fft2(impulse_response);
ir_s = fftshift(ir);
figure, imagesc([-pi pi], [-pi pi], abs(ir_s)), title("ir s");

%% ZADANIE 6
all_pass_filter = mask(pi, filter_rank);
high_pass_filter = all_pass_filter - low_pass_filter;


x2_only = zeros(N1, N2);
for n1 = 1:N1
    for n2 = 1:N2
        x2_only(n1, n2) = x2(n1, n2, k3, k4, N1, N2);
    end
end
figure, imshow([x, filter2(high_pass_filter,x), x2_only]), title("x, x bez skladowej x1, x2 only");



%% FUNKCJE DO ZADAŃ
% DO ZADANIA 1
% definicja x1(n1,n2)
function [x] = x1(n1, n2, k1, k2, N1, N2)
    x = cos(((2*pi*k1*n1)/N1) + ((2*pi*k2*n2)/N2));
end

% definicja x2(n1,n2)
function [x] = x2(n1, n2, k3, k4, N1, N2)
    x = cos(((2*pi*k3*n1)/N1) + ((2*pi*k4*n2)/N2));
end